# load array into a bash array
# need to output each entry as a single line
readarray people < <(yq e -o=j -I=0 '.[]' imgReplace.yml )

for person in "${people[@]}"; do
    # identity mapping is a yaml snippet representing a single entry
    #echo "$person"
    name=$(echo $person | yq '.name' )
    oldImg=$(echo $person | yq '.oldimg' -)
    newImg=$(echo $person | yq '.newimg' -)
    echo "item: $name , $oldImg, $newImg"
done
